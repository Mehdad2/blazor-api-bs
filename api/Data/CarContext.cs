﻿using Microsoft.EntityFrameworkCore;
using api.Model;

namespace api.Data
{
    /**
     * Db context needed for EF Core functionnality like CRUD on Models
     * We put entities necessary
     * inherit DbContext from EF core
     * 
     * In PMC  for adding a migration
     *  add-migration init
     *  
     *  In PMC create and update
     *  Update-Database
     *  
     *  In dot Cli Adding migration
     *  dotnet ef migrations add init
     *  
     *  In dot Cli create and update
     *   dotnet ef database update
     * */
    public class CarContext : DbContext
    {
        public CarContext(DbContextOptions<CarContext> options)
            : base(options)
        {
        }
        // create a Dbset property for entities set (tables), entities = row in
        //DB in EF core language
        public DbSet<CarLocationChoice> CarLChoice { get; set; }
        public DbSet<Identity> Ident { get; set; }
        public DbSet<UsageCarLocation> UsageCL { get; set; }


    }
}
