﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace api.Migrations
{
    public partial class foreign : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_CarLChoice_IdentityId",
                table: "CarLChoice",
                column: "IdentityId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_CarLChoice_Ident_IdentityId",
                table: "CarLChoice",
                column: "IdentityId",
                principalTable: "Ident",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CarLChoice_Ident_IdentityId",
                table: "CarLChoice");

            migrationBuilder.DropIndex(
                name: "IX_CarLChoice_IdentityId",
                table: "CarLChoice");
        }
    }
}
