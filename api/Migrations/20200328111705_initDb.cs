﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace api.Migrations
{
    public partial class initDb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CarLChoice",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    brand = table.Column<string>(nullable: true),
                    color = table.Column<string>(nullable: true),
                    dateStart = table.Column<DateTime>(nullable: false),
                    dateEnd = table.Column<DateTime>(nullable: false),
                    IdentityId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CarLChoice", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Ident",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FisrtName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    streetNumber = table.Column<short>(nullable: false),
                    streetAdress = table.Column<string>(nullable: true),
                    zipCode = table.Column<short>(nullable: false),
                    city = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Ident", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UsageCL",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    isShared = table.Column<bool>(nullable: false),
                    CarLocationChoiceId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UsageCL", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UsageCL_CarLChoice_CarLocationChoiceId",
                        column: x => x.CarLocationChoiceId,
                        principalTable: "CarLChoice",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_UsageCL_CarLocationChoiceId",
                table: "UsageCL",
                column: "CarLocationChoiceId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Ident");

            migrationBuilder.DropTable(
                name: "UsageCL");

            migrationBuilder.DropTable(
                name: "CarLChoice");
        }
    }
}
