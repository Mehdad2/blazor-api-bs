﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace api.Migrations
{
    public partial class required : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CarLChoice_Ident_IdentityId",
                table: "CarLChoice");

            migrationBuilder.DropIndex(
                name: "IX_CarLChoice_IdentityId",
                table: "CarLChoice");

            migrationBuilder.DropColumn(
                name: "IdentityId",
                table: "CarLChoice");

            migrationBuilder.DropColumn(
                name: "dateEnd",
                table: "CarLChoice");

            migrationBuilder.DropColumn(
                name: "dateStart",
                table: "CarLChoice");

            migrationBuilder.AddColumn<string>(
                name: "Details",
                table: "UsageCL",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "city",
                table: "Ident",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "LastName",
                table: "Ident",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "FisrtName",
                table: "Ident",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CarLocationChoiceId",
                table: "Ident",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "dateEnd",
                table: "Ident",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "dateStart",
                table: "Ident",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AlterColumn<string>(
                name: "color",
                table: "CarLChoice",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "brand",
                table: "CarLChoice",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsAvalaible",
                table: "CarLChoice",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateIndex(
                name: "IX_Ident_CarLocationChoiceId",
                table: "Ident",
                column: "CarLocationChoiceId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Ident_CarLChoice_CarLocationChoiceId",
                table: "Ident",
                column: "CarLocationChoiceId",
                principalTable: "CarLChoice",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Ident_CarLChoice_CarLocationChoiceId",
                table: "Ident");

            migrationBuilder.DropIndex(
                name: "IX_Ident_CarLocationChoiceId",
                table: "Ident");

            migrationBuilder.DropColumn(
                name: "Details",
                table: "UsageCL");

            migrationBuilder.DropColumn(
                name: "CarLocationChoiceId",
                table: "Ident");

            migrationBuilder.DropColumn(
                name: "dateEnd",
                table: "Ident");

            migrationBuilder.DropColumn(
                name: "dateStart",
                table: "Ident");

            migrationBuilder.DropColumn(
                name: "IsAvalaible",
                table: "CarLChoice");

            migrationBuilder.AlterColumn<string>(
                name: "city",
                table: "Ident",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "LastName",
                table: "Ident",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "FisrtName",
                table: "Ident",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "color",
                table: "CarLChoice",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "brand",
                table: "CarLChoice",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AddColumn<int>(
                name: "IdentityId",
                table: "CarLChoice",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "dateEnd",
                table: "CarLChoice",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "dateStart",
                table: "CarLChoice",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.CreateIndex(
                name: "IX_CarLChoice_IdentityId",
                table: "CarLChoice",
                column: "IdentityId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_CarLChoice_Ident_IdentityId",
                table: "CarLChoice",
                column: "IdentityId",
                principalTable: "Ident",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
