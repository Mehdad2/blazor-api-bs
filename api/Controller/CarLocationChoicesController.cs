﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using api.Data;
using api.Model;

namespace api.Controller
{
    [Route("api/[controller]")]
    [ApiController]
    public class CarLocationChoicesController : ControllerBase
    {
        private readonly CarContext _context;

        public CarLocationChoicesController(CarContext context)
        {
            _context = context;
        }

        // GET: api/CarLocationChoices
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CarLocationChoice>>> GetCarLChoice()
        {
            return await _context.CarLChoice.ToListAsync();
        }

        // GET: api/CarLocationChoices/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CarLocationChoice>> GetCarLocationChoice(int id)
        {
            var carLocationChoice = await _context.CarLChoice.FindAsync(id);

            if (carLocationChoice == null)
            {
                return NotFound();
            }

            return carLocationChoice;
        }

        // PUT: api/CarLocationChoices/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCarLocationChoice(int id, CarLocationChoice carLocationChoice)
        {
            if (id != carLocationChoice.Id)
            {
                return BadRequest();
            }

            _context.Entry(carLocationChoice).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CarLocationChoiceExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/CarLocationChoices
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<CarLocationChoice>> PostCarLocationChoice(CarLocationChoice carLocationChoice)
        {
            _context.CarLChoice.Add(carLocationChoice);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCarLocationChoice", new { id = carLocationChoice.Id }, carLocationChoice);
        }

        // DELETE: api/CarLocationChoices/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<CarLocationChoice>> DeleteCarLocationChoice(int id)
        {
            var carLocationChoice = await _context.CarLChoice.FindAsync(id);
            if (carLocationChoice == null)
            {
                return NotFound();
            }

            _context.CarLChoice.Remove(carLocationChoice);
            await _context.SaveChangesAsync();

            return carLocationChoice;
        }

        private bool CarLocationChoiceExists(int id)
        {
            return _context.CarLChoice.Any(e => e.Id == id);
        }
    }
}
