﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using api.Data;
using api.Model;

namespace api.Controller
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsageCarLocationsController : ControllerBase
    {
        private readonly CarContext _context;

        public UsageCarLocationsController(CarContext context)
        {
            _context = context;
        }

        // GET: api/UsageCarLocations
        [HttpGet]
        public async Task<ActionResult<IEnumerable<UsageCarLocation>>> GetUsageCL()
        {
            return await _context.UsageCL.ToListAsync();
        }

        // GET: api/UsageCarLocations/5
        [HttpGet("{id}")]
        public async Task<ActionResult<UsageCarLocation>> GetUsageCarLocation(int id)
        {
            var usageCarLocation = await _context.UsageCL.FindAsync(id);

            if (usageCarLocation == null)
            {
                return NotFound();
            }

            return usageCarLocation;
        }

        // PUT: api/UsageCarLocations/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUsageCarLocation(int id, UsageCarLocation usageCarLocation)
        {
            if (id != usageCarLocation.Id)
            {
                return BadRequest();
            }

            _context.Entry(usageCarLocation).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UsageCarLocationExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/UsageCarLocations
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<UsageCarLocation>> PostUsageCarLocation(UsageCarLocation usageCarLocation)
        {
            _context.UsageCL.Add(usageCarLocation);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetUsageCarLocation", new { id = usageCarLocation.Id }, usageCarLocation);
        }

        // DELETE: api/UsageCarLocations/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<UsageCarLocation>> DeleteUsageCarLocation(int id)
        {
            var usageCarLocation = await _context.UsageCL.FindAsync(id);
            if (usageCarLocation == null)
            {
                return NotFound();
            }

            _context.UsageCL.Remove(usageCarLocation);
            await _context.SaveChangesAsync();

            return usageCarLocation;
        }

        private bool UsageCarLocationExists(int id)
        {
            return _context.UsageCL.Any(e => e.Id == id);
        }
    }
}
