﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using api.Data;
using api.Model;

namespace api.Controller
{
    [Route("api/[controller]")]
    [ApiController]
    public class IdentitiesController : ControllerBase
    {
        private readonly CarContext _context;

        public IdentitiesController(CarContext context)
        {
            _context = context;
        }

        // GET: api/Identities
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Identity>>> GetIdent()
        {
            return await _context.Ident.ToListAsync();
        }

        // GET: api/Identities/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Identity>> GetIdentity(int id)
        {
            var identity = await _context.Ident.FindAsync(id);

            if (identity == null)
            {
                return NotFound();
            }

            return identity;
        }

        // PUT: api/Identities/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutIdentity(int id, Identity identity)
        {
            if (id != identity.Id)
            {
                return BadRequest();
            }

            _context.Entry(identity).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!IdentityExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Identities
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<Identity>> PostIdentity(Identity identity)
        {
            _context.Ident.Add(identity);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetIdentity", new { id = identity.Id }, identity);
        }

        /// <summary>
        /// Deletes a specific TodoItem.
        /// </summary>
        /// <param name="id"></param>   
        // DELETE: api/Identities/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Identity>> DeleteIdentity(int id)
        {
            var identity = await _context.Ident.FindAsync(id);
            if (identity == null)
            {
                return NotFound();
            }

            _context.Ident.Remove(identity);
            await _context.SaveChangesAsync();

            return identity;
        }

        private bool IdentityExists(int id)
        {
            return _context.Ident.Any(e => e.Id == id);
        }
    }
}
