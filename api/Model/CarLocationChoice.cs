﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
namespace api.Model
{
    public class CarLocationChoice
    {
        public int Id { get; set; }
        [Required]
        public String brand { get; set; }
        [Required]
        public String color { get; set; }
        [Required]
        public Boolean IsAvalaible { get; set; }
     
        public Identity Identy { get; set; }
        public UsageCarLocation CarUCL { get; set; }
    }
}
