﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api.Model
{
    public class UsageCarLocation
    {
        public int Id { get; set; }
        public Boolean isShared { get; set; }
        public String Details { get; set; }

        public int CarLocationChoiceId { get; set; }

}
}
