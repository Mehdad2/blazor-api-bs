﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
namespace api.Model
{
    public class Identity
    {
        public int Id { get; set; }
        [Required]
        public String FisrtName { get; set; }
        [Required]
        public String LastName { get; set; }
        public Int16 streetNumber { get; set; }
        public String streetAdress { get; set; }
        public Int16 zipCode { get; set; }
        [Required]
        public String city { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime dateStart { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime dateEnd { get; set; }
        public int CarLocationChoiceId { get; set; }
    }
}
